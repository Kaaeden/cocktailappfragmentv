package com.example.cocktailmenu

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DrinkApp: Application()