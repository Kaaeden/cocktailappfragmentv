package com.example.cocktailmenu.views

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import com.example.cocktailmenu.R
import com.example.cocktailmenu.model.local.entities.CategoryName

@Composable
fun CategoryItem(menuData: List<CategoryName>?) {

    LazyColumn(){
        if (menuData != null)
        {
            items(menuData.size)
            {
                category ->
              //  Button(onClick = { findNavController().navigate(R.layout.fragment_drink_item_list)}){
                    Text(text = menuData[category].strCategory)
               // }

            }
        }
    }

}