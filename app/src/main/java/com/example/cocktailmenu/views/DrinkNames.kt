package com.example.cocktailmenu.views

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.example.cocktailmenu.model.local.entities.DrinkItem

@Composable
fun DrinkNames(navFunc: () -> Unit,drinkList: List<DrinkItem>) {

    LazyColumn(){items(drinkList){  drinkData -> Button(onClick = {navFunc}){ Text(drinkData.strDrink)} } }

}