package com.example.cocktailmenu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import coil.compose.AsyncImage
import com.example.cocktailmenu.viewmodel.DrinkQueryViewModel
import com.example.cocktailmenu.views.CategoryItem
import com.example.cocktailmenu.views.DrinkNames
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class drinkItemList : Fragment() {

   private val drinkMenuViewModel by activityViewModels<DrinkQueryViewModel>()
    var categoryQuery : String = "Cocktail"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        println(arguments?.getString("categoryName"))
        drinkMenuViewModel.getDrinkItems(arguments?.getString("categoryName").toString())
        return ComposeView(requireContext()).apply{
        setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
        setContent {
            val state = drinkMenuViewModel.state.collectAsState()

            LazyColumn{
                items(state.value.drinkList){  drinkData -> Box() {

                    Column() {
                        AsyncImage(
                            model = drinkData.strDrinkThumb,
                            contentDescription = "Excellently Made " + drinkData.strDrink
                        )
                    Button(onClick =
                    {
                        val bundle: Bundle = Bundle()
                        bundle.putString("drinkThumb", drinkData.strDrinkThumb)
                        bundle.putString("drinkId", drinkData.idDrink)
                        findNavController().navigate(R.id.drinkProfile, bundle)
                    }) {
                        Text(drinkData.strDrink)
                    }
                }
                }

                }
            }

        }

    }

    }

}