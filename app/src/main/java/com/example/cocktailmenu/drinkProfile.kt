package com.example.cocktailmenu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import coil.compose.AsyncImage
import com.example.cocktailmenu.viewmodel.DrinkQueryViewModel
import com.example.cocktailmenu.views.DrinkNames
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class drinkProfile : Fragment() {

    private val drinkMenuViewModel by activityViewModels<DrinkQueryViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        drinkMenuViewModel.getDrinkDetails(arguments?.getString("drinkId").toString())

        return ComposeView(requireContext()).apply{
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = drinkMenuViewModel.state.collectAsState()


                    LazyColumn()
                    {
                        items(state.value.drinkDetails)
                        {
                            details -> Box(){
                                Column()
                                {
                                    AsyncImage(model= arguments?.getString("drinkThumb").toString(), contentDescription = "Selected Drink" )
                                    Text(details.strDrink.toString())
                                }
                        }

                        }
                    }
            }

        }
    }

}