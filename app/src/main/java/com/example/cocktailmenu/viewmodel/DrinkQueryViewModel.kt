package com.example.cocktailmenu.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailmenu.model.CocktailRepo
import com.example.cocktailmenu.model.local.entities.CategoryName
import com.example.cocktailmenu.model.local.entities.Drink
import com.example.cocktailmenu.model.local.entities.DrinkItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

@HiltViewModel
class DrinkQueryViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {

    private val _state = MutableStateFlow(DrinkQueryState())
    val state get() = _state.asStateFlow()

    fun getCategories(){
        _state.update{it.copy(isLoading = true)}
        viewModelScope.launch{
            val categories = repo.getDrinksCategory("list")
            _state.update { it.copy(categoryList = categories) }
        }
        _state.update{it.copy(isLoading = false)}
    }

    fun getDrinkItems(category_: String){

        _state.update{it.copy(isLoading = true)}
        viewModelScope.launch{
            val drinkList_ = repo.getDrinkByCategory(category_)
            _state.update { it.copy(drinkList = drinkList_) }
        }
        _state.update{it.copy(isLoading = false)}

    }

    fun getDrinkDetails(drinkId: String){

        _state.update{it.copy(isLoading = true)}
        viewModelScope.launch{
            val drinkDetials_ = repo.getDrinkInfoById(drinkId)
            println(drinkDetials_[0])
            _state.update { it.copy(drinkDetails = drinkDetials_) }

        }
        _state.update{it.copy(isLoading = false)}

    }


}

