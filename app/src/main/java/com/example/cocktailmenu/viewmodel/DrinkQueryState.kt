package com.example.cocktailmenu.viewmodel

import com.example.cocktailmenu.model.local.entities.CategoryName
import com.example.cocktailmenu.model.local.entities.Drink
import com.example.cocktailmenu.model.local.entities.DrinkDetails
import com.example.cocktailmenu.model.local.entities.DrinkItem

data class DrinkQueryState(
    val categoryList: List<CategoryName>? = emptyList(),
    val drinkList: List<DrinkItem> = emptyList(),
    val drinkDetails: List<Drink> = emptyList(),
    val isLoading: Boolean = false
){}