package com.example.cocktailmenu.model

import com.example.cocktailmenu.model.local.entities.CategoryDataDTO
import com.example.cocktailmenu.model.mapper.CategoryMapper
import org.w3c.dom.CharacterData

sealed class NetworkResponse<T> {

    data class SuccessfulCategory(
        val categoryL : CategoryDataDTO
    ) : NetworkResponse<CategoryDataDTO>()

    object Loading : NetworkResponse<Unit>()

    data class Error(
        val message: String
    ) : NetworkResponse<String>()
}
