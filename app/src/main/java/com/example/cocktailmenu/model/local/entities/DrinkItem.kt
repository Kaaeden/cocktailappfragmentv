package com.example.cocktailmenu.model.local.entities

data class DrinkItem(val strDrink: String, val strDrinkThumb: String, val idDrink: String)
