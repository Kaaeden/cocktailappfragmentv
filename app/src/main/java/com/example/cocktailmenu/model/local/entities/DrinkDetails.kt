package com.example.cocktailmenu.model.local.entities

import com.example.cocktailmenu.model.dtos.DrinkDTO
@kotlinx.serialization.Serializable
data class DrinkDetails(
    val drinks: List<DrinkDTO>
)