package com.example.cocktailmenu.model.dtos

data class DrinkItemDTO(val strDrink: String, val strDrinkThumb: String, val idDrink: String)