package com.example.cocktailmenu.model.mapper

sealed interface Mapper<in DTO, out ENTITY> {
        operator fun invoke(dto: DTO): ENTITY
    }
