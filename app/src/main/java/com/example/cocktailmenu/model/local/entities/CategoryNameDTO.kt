package com.example.cocktailmenu.model.local.entities

import kotlinx.serialization.Serializable

@Serializable
data class CategoryNameDTO(
        val strCategory: String
)