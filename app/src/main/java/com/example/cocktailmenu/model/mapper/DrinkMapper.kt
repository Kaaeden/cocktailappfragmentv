package com.example.cocktailmenu.model.mapper

import com.example.cocktailmenu.model.dtos.DrinkDTO
import com.example.cocktailmenu.model.dtos.DrinkItemDTO
import com.example.cocktailmenu.model.local.entities.Drink
import com.example.cocktailmenu.model.local.entities.DrinkItem

class DrinkMapper: Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink = with(dto) {
        return Drink(dateModified = dateModified,  idDrink =  idDrink, strImageSource = strImageSource, strInstructions = strInstructions, strDrink = strDrink)
    }
}