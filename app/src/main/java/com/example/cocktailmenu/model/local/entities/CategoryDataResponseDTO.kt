package com.example.cocktailmenu.model.local.entities

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDataResponseDTO(
    val drinks: List<CategoryNameDTO>
)

