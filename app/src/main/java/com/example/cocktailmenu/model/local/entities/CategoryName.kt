package com.example.cocktailmenu.model.local.entities
@kotlinx.serialization.Serializable
data class CategoryName(
    val strCategory: String
)