package com.example.cocktailmenu.model

//import com.example.cocktailmenu.model.local.entities.CategoryData
import com.example.cocktailmenu.model.local.entities.CategoryDataResponseDTO
import com.example.cocktailmenu.model.local.entities.CategoryName
import com.example.cocktailmenu.model.local.entities.Drink
import com.example.cocktailmenu.model.local.entities.DrinkCabinet
import com.example.cocktailmenu.model.local.entities.DrinkDetails
import com.example.cocktailmenu.model.local.entities.DrinkItem
import com.example.cocktailmenu.model.mapper.CategoryMapper
import com.example.cocktailmenu.model.mapper.DrinkItemMapper
import com.example.cocktailmenu.model.mapper.DrinkMapper
import javax.inject.Inject
import kotlinx.coroutines.withContext
import retrofit2.Response

class CocktailRepo @Inject constructor(private val service: CocktailService) {

    private val catMapper: CategoryMapper = CategoryMapper()
    private val drinkMapper: DrinkItemMapper = DrinkItemMapper()
    private val drinkDetailMap: DrinkMapper = DrinkMapper()
    suspend fun getDrinksCategory(c: String): List<CategoryName>? {
            val repoResponse: CategoryDataResponseDTO = service.getDrinkCategories()
            return repoResponse.drinks.map{catMapper(it)}

    }

   suspend fun  getDrinkByCategory(c: String): List<DrinkItem>
    {

        val repoResponse: DrinkCabinet = service.getDrinksByCategory(c)
        return repoResponse.drinks.map{drinkMapper(it)}


    }

    suspend fun getDrinkInfoById(id: String): List<Drink> {

        val repoResponse: DrinkDetails = service.getDrinkInfoById(id)
        val queryResponse: List<Drink> = repoResponse.drinks.map{ drinkDetailMap(it) }
        return queryResponse

    }

}