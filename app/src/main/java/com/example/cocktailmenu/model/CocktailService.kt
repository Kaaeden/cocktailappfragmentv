package com.example.cocktailmenu.model

//import com.example.cocktailmenu.model.local.entities.CategoryData
import com.example.cocktailmenu.model.dtos.DrinkDTO
import com.example.cocktailmenu.model.dtos.DrinkItemDTO
import com.example.cocktailmenu.model.local.entities.CategoryDataResponseDTO
import com.example.cocktailmenu.model.local.entities.DrinkCabinet
import com.example.cocktailmenu.model.local.entities.DrinkDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailService {

    @GET("list.php")
    suspend fun getDrinkCategories(@Query("c") categoryQuery: String = "list") : CategoryDataResponseDTO
    //www.thecocktaildb.com/api/json/v1/1/list.php?c=List

    @GET("filter.php")
    suspend fun getDrinksByCategory(@Query("c") drinkCategory: String = "list" ) : DrinkCabinet
    //www.thecocktaildb.com/api/json/v1/1/filter.php?c=Category

    @GET("lookup.php")
    suspend fun getDrinkInfoById(@Query("i") drinkId: String) : DrinkDetails
    //www.thecocktaildb.com/api/json/v1/1/lookup.php?i=11007
}