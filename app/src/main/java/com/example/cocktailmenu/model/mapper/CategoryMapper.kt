package com.example.cocktailmenu.model.mapper

import com.example.cocktailmenu.model.local.entities.CategoryNameDTO
import com.example.cocktailmenu.model.local.entities.CategoryName

class CategoryMapper: Mapper<CategoryNameDTO, CategoryName> {
    override fun invoke(dto: CategoryNameDTO): CategoryName = with(dto) {

        CategoryName(strCategory = strCategory)
    }

}