package com.example.cocktailmenu.model.mapper

import com.example.cocktailmenu.model.dtos.DrinkItemDTO
import com.example.cocktailmenu.model.local.entities.DrinkItem

class DrinkItemMapper: Mapper<DrinkItemDTO, DrinkItem> {

    override fun invoke(dto: DrinkItemDTO): DrinkItem = with(dto) {

        DrinkItem(strDrink = strDrink, strDrinkThumb = strDrinkThumb, idDrink = idDrink)
    }
}