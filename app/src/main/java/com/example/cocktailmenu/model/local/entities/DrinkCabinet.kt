package com.example.cocktailmenu.model.local.entities

import com.example.cocktailmenu.model.dtos.DrinkItemDTO

data class DrinkCabinet(val drinks: List<DrinkItemDTO>)
