package com.example.cocktailmenu.model.local.entities
@kotlinx.serialization.Serializable
data class CategoryDataDTO(
    val drinks: List<CategoryName>
)