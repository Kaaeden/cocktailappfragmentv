package com.example.cocktailmenu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.cocktailmenu.model.CocktailRepo
import com.example.cocktailmenu.model.local.entities.CategoryName
import com.example.cocktailmenu.viewmodel.DrinkQueryState
import com.example.cocktailmenu.viewmodel.DrinkQueryViewModel
import com.example.cocktailmenu.views.CategoryItem
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class main_drink_list : Fragment() {
    private val drinkViewModel by activityViewModels<DrinkQueryViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        drinkViewModel.getCategories()
        return ComposeView(requireContext()).apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = drinkViewModel.state.collectAsState()
                // CategoryItem(menuData = state.value.categoryList)
                LazyColumn() {
                    val drinkNames: List<CategoryName> = state.value.categoryList ?: emptyList()

                    if (state.value.categoryList != null) {
                        items(drinkNames) { category ->
                            Button(onClick =
                            {
                                val bundle : Bundle = Bundle()
                                bundle.putString("categoryName", category.strCategory)
                                println(category.strCategory)
                                findNavController().navigate(R.id.drinkItemList, bundle) }) {
                                Text(text = category.strCategory)
                            }
                        }

                    }
                }
            }

        }
    }

}